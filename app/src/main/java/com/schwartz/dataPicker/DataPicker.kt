package com.schwartz.dataPicker

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DataPicker(private val fragment: Fragment) {

    private var fileUri: Uri? = null
    private var state: DataPickerState = DataPickerState.EMPTY
    private var dataCallback: ((DataPickerResult) -> Unit)? = null

    fun openCamera(callback: (DataPickerResult) -> Unit) {
        fileUri = null
        dataCallback = callback
        state = DataPickerState.CAMERA
        openCameraForResult()
    }

    fun openGallery(callback: (DataPickerResult) -> Unit) {
        dataCallback = callback
        state = DataPickerState.GALLERY
        openGalleryForResult()
    }

    fun openFileManager(callback: (DataPickerResult) -> Unit) {
        dataCallback = callback
        state = DataPickerState.GALLERY
        openFileManagerForResult()
    }

    private fun openCameraForResult() {
        if (hasPermission().not()) askPermission()
        else fileUri = createTakePictureIntent(fragment, activityResult)
    }

    private fun openGalleryForResult() {
        if (hasPermission().not()) askPermission()
        else activityResult.launch(fragment.requireContext().createSelectPhotoIntent())
    }

    private fun openFileManagerForResult() {
        if (hasPermission().not()) askPermission()
        else activityResult.launch(createSelectFileIntent())
    }

    private fun hasPermission(): Boolean {
        val context = fragment.requireContext()
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    private fun askPermission() {
        permissionResult.launch(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
            )
        )
    }

    private val permissionResult by lazy {
        fragment.requireActivity().activityResultRegistry.register(
            PERMISSION_KEY,
            ActivityResultContracts.RequestMultiplePermissions()
        ) { result ->
            result?.let { permissionResult ->
                if (permissionResult.all { it.value == true }) {
                    when (state) {
                        DataPickerState.GALLERY -> openGalleryForResult()
                        DataPickerState.CAMERA -> openCameraForResult()
                        DataPickerState.FILE -> openFileManagerForResult()
                        else -> Log.e(TAG,"DataPicker wrong state: $state")
                    }
                }
            }
        }
    }

    private val activityResult by lazy {
        fragment.requireActivity().activityResultRegistry.register(
            DATA_PICKER_RESULT_KEY,
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            result?.let { activityResult ->
                if (activityResult.resultCode == Activity.RESULT_OK) {
                    when (state) {
                        DataPickerState.GALLERY -> dataCallback?.invoke(DataPickerResult(state, result.data?.data))
                        DataPickerState.CAMERA -> dataCallback?.invoke(DataPickerResult(state, fileUri))
                        DataPickerState.FILE -> dataCallback?.invoke(DataPickerResult(state, result.data?.data))
                        else -> Log.e(TAG, "DataPicker wrong state: $state")
                    }
                } else dataCallback?.invoke(DataPickerResult(DataPickerState.CANCELED, result.data?.data))
            }
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private fun createTakePictureIntent(fragment: Fragment, activityResult: ActivityResultLauncher<Intent>): Uri? {
        var targetURI: Uri? = null
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val activity = fragment.requireActivity()
        val packageManager = activity.packageManager
        if (takePictureIntent.resolveActivity(packageManager) != null)
            try {
                targetURI = FileProvider.getUriForFile(activity, AUTHORITY, createImageFile(activity))
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, targetURI)
                activityResult.launch(takePictureIntent)
            } catch (exception: IOException) {
                Log.e(TAG, "createTakePictureIntent", exception)
                return null
            }
        return targetURI
    }

    private fun createSelectFileIntent() =
        Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = MimeType.DEFAULT
            putExtra(
                Intent.EXTRA_MIME_TYPES,
                arrayOf(
                    MimeType.IMAGE_JPEG, MimeType.IMAGE_PNG,
                    MimeType.APPLICATION_PDF, MimeType.TEXT_DOCX, MimeType.TEXT_DOC,
                    MimeType.SHEET_XLS, MimeType.SHEET_XLSX
                )
            )
        }

    private fun createImageFile(activity: Activity): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val picturesDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile("JPEG_${timeStamp}_", ".jpg", picturesDir)
    }

    companion object {
        const val TAG = "DataPicker"
        const val PERMISSION_KEY = "permission_key"
        const val DATA_PICKER_RESULT_KEY = "data_picker_result_key"
        const val AUTHORITY = BuildConfig.APPLICATION_ID + ".android.fileprovider"
    }
}

data class DataPickerResult(
    val dataPickerState: DataPickerState = DataPickerState.EMPTY,
    val uri: Uri? = null,
)

enum class DataPickerState { GALLERY, CAMERA, FILE, EMPTY, CANCELED }
