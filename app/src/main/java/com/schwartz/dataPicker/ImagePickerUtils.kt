package com.schwartz.dataPicker

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log

private const val GOOGLE_PHOTOS_PACKAGE = "com.google.android.apps.photos"

/**
 * @param allowChooseTheApp if true, will scan through all the apps and open Intent chooser
 *
 * otherwise will check if Google Photos is installed and open it. If not installed, default files dialog will be shown
 */
fun Context.createSelectPhotoIntent(allowChooseTheApp: Boolean = false): Intent =
    Intent(Intent.ACTION_GET_CONTENT).apply {
        type = "image/*"
    }.let { defaultIntent ->
        if (!allowChooseTheApp) {
            if (isGooglePhotosInstalled()) defaultIntent.setPackage(GOOGLE_PHOTOS_PACKAGE)
            return defaultIntent
        }

        val extraIntents = getSuitableIntents(defaultIntent)
        if (extraIntents.isNotEmpty())
            Intent.createChooser(defaultIntent, "Select an app").apply {
                putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents.toTypedArray())
            }
        else defaultIntent
    }

private fun Context.getSuitableIntents(intent: Intent) =
    packageManager.queryIntentActivities(intent, 0).map {
        Intent(intent).apply {
            setPackage(it.activityInfo.packageName)
        }
    }

fun Context.isGooglePhotosInstalled() = isAppInstalled(GOOGLE_PHOTOS_PACKAGE)

fun Context.isAppInstalled(packageName: String) = try {
    packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES)
    true
} catch (e: PackageManager.NameNotFoundException) {
    false
} catch (e: Exception) {
    Log.e("isAppInstalled", "isAppInstalled", e)
    false
}