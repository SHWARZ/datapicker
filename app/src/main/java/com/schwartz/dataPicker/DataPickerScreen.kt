package com.schwartz.dataPicker

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment

class DataPickerScreen : Fragment(R.layout.data_picker_screen) {

    private val dataPicker = DataPicker(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val imageView = view.findViewById<ImageView>(R.id.imageView)
        val textView = view.findViewById<TextView>(R.id.textView)

        imageView.setOnClickListener { dataPicker.openGallery { textView.text = it.uri.toString() } }
    }
}