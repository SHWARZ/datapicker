package com.schwartz.dataPicker

object MimeType {
    const val APPLICATION_PDF = "application/pdf"
    const val IMAGE_JPEG = "image/jpeg"
    const val IMAGE_PNG = "image/png"
    const val APLICATION_EXCEL = "application/excel"
    const val TEXT_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
    const val TEXT_DOC = "application/msword"
    const val SHEET_XLS = "application/vnd.ms-excel"
    const val SHEET_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    const val TEXT_CSV = "text/csv"
    const val TEXT_HTML = "text/html"
    const val DEFAULT = "*/*"
}